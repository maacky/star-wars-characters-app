# star-wars-characters-app

A simple Star Wars characters search app using [SWAPI](https://swapi.co/) and written in [React](https://reactjs.org/) and [Typescript](https://www.typescriptlang.org/docs/home.html).

## Usage

To see the app, run
```javascript
git clone https://maacky@bitbucket.org/maacky/star-wars-characters-app.git
cd star-wars-characters-app
yarn
yarn start
```

For testing, run `yarn test` and check the terminal for report.

