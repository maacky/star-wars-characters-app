import { CHARACTER_URL_BASE } from './consts'
import { ICharacterData } from '../interfaces/character'

export const fetchCharacters = (value: string): Promise<ICharacterData[]> => 
  fetch(`${CHARACTER_URL_BASE}?search=${value}`)
    .then(res => res.json())
    .then(res => res.results)