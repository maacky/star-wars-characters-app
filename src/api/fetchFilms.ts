import { FILMS_URL } from './consts'
import { IFilmData } from '../interfaces/film'

export const fetchFilms = (): Promise<IFilmData[]> => fetch(FILMS_URL)
  .then(res => res.json())
  .then(res => res.results)
  