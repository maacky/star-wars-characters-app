import * as React from 'react'
import styled from 'styled-components'

import { Input } from './Input'
import { Result } from './Result'
import { debounce } from '../utils/debounce'
import { fetchCharacters } from '../api/fetchCharacters'
import { getCharacterFilms } from '../utils/getCharacterFilms'
import { fetchFilms } from '../api/fetchFilms'
import { getFilmDetails } from '../utils/getFilmDetails'
import { getCharacterDetails } from '../utils/getCharacterDetails'
import { colors } from '../consts/colors'
import { IFilmState } from '../interfaces/film'
import { ICharacterResult } from '../interfaces/character'

const ERROR_MESSAGE = 'I feel disturbance in the force. Reload the page or drop in later.'

const App: React.FunctionComponent = () => {
  const [searchResult, setSearchResult] = React.useState<ICharacterResult[] | null>(null)
  const [isLoading, setIsLoading] = React.useState<boolean>(false)
  const [films, setFilms] = React.useState<IFilmState | null>(null)
  const [characterQuery, setCharacterQuery] = React.useState<string>('')
  const [error, setError] = React.useState<string | null>(null)

  const setCharacterData = async (value: string, filmData: IFilmState) => {
    if (value) {
      setIsLoading(true)
  
      const characterData = await fetchCharacters(value)
      const characterDetails = getCharacterDetails(characterData).map(
        character => getCharacterFilms(character, filmData),
      )
  
      setSearchResult(characterDetails)
      setIsLoading(false)
    }

    else {
      setSearchResult(null)
    }
  }

  const setCharacterDataDebounced = React.useRef(debounce(setCharacterData, 700))

  React.useEffect(
    () => {
      const setFilmData = async () => {
        const filmData = await fetchFilms()
        const filmDetails = getFilmDetails(filmData)

        setFilms(filmDetails)
        setIsLoading(false)
      }

      try {
        setIsLoading(true)
        setFilmData()
      }

      catch {
        setIsLoading(false)
        setError(ERROR_MESSAGE)
      }
    },
    [],
  )

  React.useEffect(
    () => {
      try {
        setCharacterDataDebounced.current(characterQuery, films)
      }

      catch {
        setIsLoading(false)
        setError(ERROR_MESSAGE)
      }
    },
    [characterQuery],
  )

  return (
    <AppWrapper>
      <H1>Welcome to the first Star Wars app ever written!</H1>
      {films && Object.values(films).length ? (
        <Input
          handleNewInput={setCharacterQuery}
          disabled={isLoading}
        />
      ) : null}
      <Result data={searchResult} error={error} loading={isLoading} />
    </AppWrapper>
  )
}

export default App

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family: 'Roboto', sans-serif;
  color: ${colors.darkGray};
`

const H1 = styled.h1`
  font-family: 'Raleway', sans-serif;
`