import * as React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'

import { Character } from './'

describe('Character component', () => {
  it('should match snapshot', () => {
    const character = {
      birthYear: 'someYear',
      eyeColor: 'someColor',
      films: [
        {
          director: 'someDirector',
          releaseDate: 'someDate',
          title: 'someTitle',
        },
      ],
      gender: 'someGender',
      hairColor: 'someColor',
      height: 'someHeight',
      name: 'someName',
    }

    const { container } = render(<Character character={character} />)

    expect(container.firstChild).toMatchSnapshot()
  })
})