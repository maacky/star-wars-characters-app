import * as React from 'react'
import styled from 'styled-components'

import { colors } from '../../consts/colors'
import { IResult } from '../Result'

interface ICharacter {
  character: IResult['data'][number]
} 

export const Character: React.FunctionComponent<ICharacter> = ({ character }) => {
  const { birthYear, eyeColor, films, gender, hairColor, height, name } = character
  
  return (
    <CharacterWrapper>
      <Name>{name}</Name>
      <DetailsWrapper>
        <CharacterInfo>Birth year: {birthYear}</CharacterInfo>
        <CharacterInfo>Gender: {gender}</CharacterInfo>
        <CharacterInfo>Height: {height}</CharacterInfo>
        <CharacterInfo>Eye color: {eyeColor}</CharacterInfo>
        <CharacterInfo>Hair color: {hairColor}</CharacterInfo>
      </DetailsWrapper>
      <DetailsWrapper>
        {films && films.map((film, i) => (
          <FilmInfo key={i + film.releaseDate}>
            <Film>{film.title}</Film> ({new Date(film.releaseDate).getFullYear()}, dir. {film.director})
          </FilmInfo>
        ))}
      </DetailsWrapper>
    </CharacterWrapper>
  )
}

const CharacterWrapper = styled.div.attrs({
  'data-testid': 'character'
})`
  width: 500px;
  padding: 10px 10px 20px 10px;
  border-bottom: 1px solid ${colors.darkGray};
  margin-top: 20px;

  &:last-child {
    border: none;
  }
`

const Name = styled.div`
  font-size: 22px;
`

const DetailsWrapper = styled.div`
  margin-top: 20px;
`

const Info = styled.div`
  margin-top: 5px;
`

const CharacterInfo = styled(Info)`
  color: ${colors.gray};
`

const FilmInfo = styled(Info)`
  margin-top: 5px;
  color: ${colors.darkGray};
`

const Film = styled.span`
  font-style: italic;
`