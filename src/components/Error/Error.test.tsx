import * as React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'

import { Error } from './'

describe('Error component', () => {
  it('should match snapshot', () => {
    const { container } = render(<Error error='someError' />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should contain text "someError" when prop of same value is passed', () => {
    const { container } = render(<Error error='someError' />)

    expect(container.firstChild).toHaveTextContent('someError')
  })
})