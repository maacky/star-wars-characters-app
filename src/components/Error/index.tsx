import * as React from 'react'
import styled from 'styled-components'

import { colors } from '../../consts/colors'

interface IError {
  error: string
}

export const Error: React.FunctionComponent<IError> = ({ error }) => <ErrorWrapper>{error}</ErrorWrapper>

const ErrorWrapper = styled.div.attrs({
  'data-testid': 'error'
})`
  min-height: 150px;
  width: 700px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  background-color: ${colors.darkGray};
`