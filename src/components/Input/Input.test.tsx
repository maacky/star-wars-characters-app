import * as React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, cleanup, fireEvent } from '@testing-library/react'

import { Input, INPUT_PLACEHOLDER } from './'

describe('Input component', () => {
  const inputProps = {
    disabled: false,
    handleNewInput: () => false,
    value: 'someValue',
  }

  beforeEach(cleanup)

  it('should match snapshot when not disabled', () => {
    const { container } = render(<Input {...inputProps} />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should match snapshot when disabled', () => {
    const inputPropsDisabled = {
      disabled: true,
      handleNewInput: () => false,
      value: 'someValue',
    }

    const { container } = render(<Input {...inputPropsDisabled} />)

    expect(container.firstChild).toMatchSnapshot()
  })

  it('should call onChange callback on input change', () => {
    const mockCallbackFunc = jest.fn()
    
    const inputPropsMockCallback = {
      disabled: false,
      handleNewInput: mockCallbackFunc,
      value: ''
    }

    const { getByPlaceholderText } = render(<Input {...inputPropsMockCallback} />)
    const input = getByPlaceholderText(INPUT_PLACEHOLDER)

    expect(mockCallbackFunc).not.toHaveBeenCalled()

    fireEvent.change(input, { target: { value: 'someValue' } })
    
    expect(mockCallbackFunc).toHaveBeenCalledTimes(1)
  })
})