import * as React from 'react'
import styled from 'styled-components'

import { colors } from '../../consts/colors' 

export const INPUT_PLACEHOLDER = 'Type character name'

interface IInput {
  disabled: boolean
  handleNewInput: (value: string) => void
}

export const Input: React.FunctionComponent<IInput> = ({ handleNewInput, disabled }) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    handleNewInput(e.target.value)
  }

  return (
    <InputWrapper>
      <InputStyled
        type='text'
        placeholder='Type character name'
        disabled={disabled}
        onChange={handleChange}
      />
    </InputWrapper>
  )
}

const InputStyled = styled.input`
  border: 1px solid ${colors.gray};
  margin: 0 auto;
  height: 20px;
  width: 300px;
  line-height: 20px;
  padding: 8px;

  &:disabled {
    background-color: inherit;
  }

  &:focus {
    outline: none;
  }
`

const InputWrapper = styled.div`
  position: relative;
  cursor: pointer;
`