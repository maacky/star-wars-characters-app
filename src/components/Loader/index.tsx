import styled, { keyframes } from 'styled-components'

import { colors } from '../../consts/colors'

const spin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`

export const Loader = styled.div.attrs({
  'data-testid': 'loader'
})`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  position: relative;
  border-top: 5px solid ${colors.gray};
  border-right: 5px solid ${colors.gray};
  border-bottom: 5px solid ${colors.gray};
  border-left: 5px solid ${colors.darkGray};
  animation: ${spin} .5s linear infinite;
`