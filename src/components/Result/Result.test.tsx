import * as React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, cleanup } from '@testing-library/react'

import { Result } from './'
import { ICharacterResult } from '../../interfaces/character'

describe('Result component', () => {
  beforeEach(cleanup)

  it('should render only Error component when loading is true', () => {
    const resultPropsLoading = {
      data: [] as ICharacterResult[],
      error: 'someError',
      loading: true,
    }

    const { getByTestId, queryByTestId } = render(<Result {...resultPropsLoading} />)

    expect(getByTestId('error')).toBeInTheDocument()
    expect(queryByTestId('loader')).not.toBeInTheDocument()
    expect(queryByTestId('no-data')).not.toBeInTheDocument()
    expect(queryByTestId('character')).not.toBeInTheDocument()
  })

  it('should render only Loader component when loading is true', () => {
    const resultPropsLoading = {
      data: [] as ICharacterResult[],
      loading: true,
    }

    const { getByTestId, queryByTestId } = render(<Result {...resultPropsLoading} />)

    expect(getByTestId('loader')).toBeInTheDocument()
    expect(queryByTestId('no-data')).not.toBeInTheDocument()
    expect(queryByTestId('character')).not.toBeInTheDocument()
    expect(queryByTestId('error')).not.toBeInTheDocument()
  })

  it('should return null when loading is set to false and no data is passed', () => {
    const resultPropsNull = {
      loading: false,
    }

    const { container } = render(<Result {...resultPropsNull} />)

    expect(container.firstChild).toBeNull()
  })

  it('should return only characters when loading is set to false and data is provided', () => {
    const resultPropsCharacters = {
      data: [{ birthYear: 'someYear', name: 'someName' }] as ICharacterResult[],
      loading: false,
    }

    const { getByTestId, queryByTestId } = render(<Result {...resultPropsCharacters} />)

    expect(getByTestId('character')).toBeInTheDocument()
    expect(queryByTestId('loader')).not.toBeInTheDocument()
    expect(queryByTestId('no-data')).not.toBeInTheDocument()
    expect(queryByTestId('error')).not.toBeInTheDocument()
  })

  it('should return only NoData component when loading is set to false and empty data array is passed', () => {
    const resultPropsNoData = {
      data: [] as ICharacterResult[],
      loading: false,
    }

    const { getByTestId, queryByTestId } = render(<Result {...resultPropsNoData} />)

    expect(getByTestId('no-data')).toBeInTheDocument()
    expect(queryByTestId('loader')).not.toBeInTheDocument()
    expect(queryByTestId('character')).not.toBeInTheDocument()
    expect(queryByTestId('error')).not.toBeInTheDocument()
  })
})