import * as React from 'react'
import styled from 'styled-components'

import { Character } from '../Character'
import { Error } from '../Error'
import { Loader } from '../Loader'
import { ICharacterResult } from '../../interfaces/character'

const NO_DATA_MESSAGE = 'No data was found. Try searching someone else!'

export interface IResult {
  data?: ICharacterResult[] | null
  error?: string | null
  loading: boolean
}

export const Result: React.FunctionComponent<IResult> = ({ data, error, loading }) => {
  const getResultContent = (): React.ReactElement => {
    if (error) {
      return <Error error={error} />
    }
    
    if (loading) {
      return <Loader />
    }

    if (!data) {
      return null
    }

    if (data.length) {
      return (
        <React.Fragment>
          {data.map(result => <Character character={result} key={result.name} />)}
        </React.Fragment>
      )
    }

    return <NoData>{NO_DATA_MESSAGE}</NoData>
  }

  const resultContent = getResultContent()

  return resultContent ? (
    <ResultWrapper>
      {resultContent}
    </ResultWrapper>
  ) : null
}

const NoData = styled.div.attrs({
  'data-testid': 'no-data'
})`
  font-size: 18px;
`

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 30px;
`