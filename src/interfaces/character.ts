import { IFilmDetails } from './film'

export interface ICharacterData {
  name: string
  height: string
  mass: string
  hair_color: string
  skin_color: string
  eye_color: string
  birth_year: string
  gender: string
  films: string[]
}

export interface ICharacterSelectedData extends Omit<
  ICharacterData,
  'mass' |
  'skin_color' |
  'birth_year' |
  'eye_color' |
  'hair_color'
> {
  birthYear: string
  eyeColor: string
  hairColor: string
}

export interface ICharacterResult extends Omit<ICharacterSelectedData, 'films'> {
  films: IFilmDetails[]
}