export interface IFilmData {
  characters: string[]
  created: string
  director: string
  edited: string
  episode_id: number
  opening_crawl: string
  planets: string[]
  producer: string
  release_date: string
  species: string[]
  starships: string[]
  title: string
  url: string
  vehicles: string
}

export interface IFilmDetails {
  director: string
  releaseDate: string
  title: string
}

export interface IFilmState {
  [url: string]: IFilmDetails
}