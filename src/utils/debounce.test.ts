import { debounce } from './debounce'

jest.useFakeTimers()

describe('utils - debounce', () => {
  it('should execute debounced function only once when function is not called again after timeout', () => {
    const func = jest.fn()
    const funcDebounced = debounce(func, 500)

    funcDebounced()
    funcDebounced()

    jest.runAllTimers()

    expect(func).toHaveBeenCalledTimes(1)
  })

  it('should execute debounced function twice when function is called again after timeout', () => {
    const func = jest.fn()
    const funcDebounced = debounce(func, 500)

    funcDebounced()
    funcDebounced()

    jest.runAllTimers()

    funcDebounced()

    jest.runAllTimers()

    expect(func).toHaveBeenCalledTimes(2)
  })
})