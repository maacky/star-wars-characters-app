export const debounce = <F extends ((...args: any) => any)>(func: F, timeout: number) => {
  let timeoutHandler: number

  return function(...args: any) {
    const funcBound = () => { func.apply(this, args) }

    clearTimeout(timeoutHandler)
    timeoutHandler = setTimeout(funcBound, timeout)
  }
}
