import { getCharacterDetails } from './getCharacterDetails'

const characters = [
  {
    birth_year: 'someYear',
    eye_color: 'someColor',
    films: ['film1', 'film2'],
    gender: 'someGender',
    hair_color: 'someColor',
    height: 'someHeight',
    mass: 'someMass',
    name: 'someName',
    skin_color: 'someColor',
  },
]

const characterDetails = [
  {
    birthYear: 'someYear',
    eyeColor: 'someColor',
    films: ['film1', 'film2'],
    gender: 'someGender',
    hairColor: 'someColor',
    height: 'someHeight',
    name: 'someName',
  },
]

describe('utils - getCharacterDetails', () => {
  it('should return array of characters with selected data', () => {
    expect(getCharacterDetails(characters)).toEqual(characterDetails)
  }) 
})