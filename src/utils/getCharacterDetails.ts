import { ICharacterData, ICharacterSelectedData } from '../interfaces/character'

export const getCharacterDetails = (charactersData: ICharacterData[]): ICharacterSelectedData[] => 
  charactersData.map(
    (character: ICharacterData): ICharacterSelectedData => {
      const { films, gender, height, name } = character

      return {
        birthYear: character.birth_year,
        eyeColor: character.eye_color,
        films,
        gender,
        hairColor: character.hair_color,
        height,
        name,
      }   
    }
  )
