import { ICharacterSelectedData, ICharacterResult } from '../interfaces/character'
import { IFilmDetails } from '../interfaces/film'
import { getCharacterFilms } from './getCharacterFilms'

const film = {} as IFilmDetails

const films = {
  'someFilm': film
}

const filmsWithNoCharacterFilm = {
  'someOtherFilm': film
}

const character = {
  films: ['someFilm'],
  name: 'someName',
} as ICharacterSelectedData

const result = {
  films: [film],
  name: 'someName',
} as ICharacterResult

const resultWithNoCharacterFilm = {
  films: [undefined],
  name: 'someName',
} as ICharacterResult

describe('utils - getCharacterFilms', () => {
  it('should return character data with full film data', () => {
    expect(getCharacterFilms(character, films)).toEqual(result)
  })

  it('should return character data with array containing undefined when no film corresponding to character film can be found', () => {
    expect(getCharacterFilms(character, filmsWithNoCharacterFilm)).toEqual(resultWithNoCharacterFilm)
  })
})