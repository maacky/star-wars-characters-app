import { ICharacterSelectedData, ICharacterResult } from '../interfaces/character'
import { IFilmState, IFilmDetails } from '../interfaces/film'

export const getCharacterFilms = (character: ICharacterSelectedData, films: IFilmState): ICharacterResult => ({
  ...character,
  films: character.films.map(
    (film: string): IFilmDetails => films[film]
  ),
})