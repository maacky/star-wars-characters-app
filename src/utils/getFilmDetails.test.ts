import { IFilmData } from '../interfaces/film'
import { getFilmDetails } from './getFilmDetails'

const filmsData = [
  {
    director: 'someDirector',
    release_date: 'someDate',
    title: 'someTitle',
    url: 'someUrl',
  },
] as IFilmData[]

const result = {
  'someUrl': {
    director: 'someDirector',
    releaseDate: 'someDate',
    title: 'someTitle',
  }
} 

describe('utils - getFilmDetails', () => {
  it('should return correct result object', () => {
    expect(getFilmDetails(filmsData)).toEqual(result)
  })
})