import { IFilmDetails, IFilmData, IFilmState } from '../interfaces/film'

export const getFilmDetails = (filmsData: IFilmData[]): IFilmState => filmsData.reduce(
  (acc: IFilmDetails | {}, curr: IFilmData) => ({
    ...acc,
    [curr.url]: {
      director: curr.director,
      releaseDate: curr.release_date,
      title: curr.title,
    },  
  }),
  {},
)